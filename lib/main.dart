import 'package:flutter/material.dart';
import 'package:flutter_app_navigation/login_route.dart';
import 'package:flutter_app_navigation/profile_route.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      //theme: ThemeData(platform: TargetPlatform.iOS),
      home: MainRoute(),
    );
  }
}


class MainRoute extends StatelessWidget {

  Widget _buildInfoWidget(BuildContext context, double w) {
    return Expanded(
      flex: 2,
      child: Container(
        width: w,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.asset('assets/ninja.png', width: 72, height: 72),
              ),
              Text(
                  'Discover upcoming events\nnear you',
                  style: TextStyle(
                      color: Colors.grey[600],
                      fontSize: 24
                  ),
                  textAlign: TextAlign.center
              ),
            ]
        ),
      ),
    );
  }

  Widget _buildButtonWidget(BuildContext context, double w) {
    return Expanded(
      flex: 1,
      child: Container(
          width: w,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              _buildRaisedButton((){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ProfileRoute(email: "Nam Pham"))
                );
              }, w),
              Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: _buildOutlineButton((){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => LoginRoute()),
                  );
                },w),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 16.0, top: 8),
                child: FlatButton(
                  onPressed: () {
                    print('skip for now');
                  },
                  child: Text(
                    'Skip for now',
                    style: TextStyle(color: Colors.blue),
                  ),
                ),
              )
            ],
          )
      ),
    );
  }

  Widget _buildRaisedButton(void onPressed(), double w) {
    return ButtonTheme(
      minWidth: w - 32*2,
      height: 48,
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(colors: <Color>[Colors.blue[300], Colors.blue[700]]),
        ),
        child: FlatButton(
          onPressed: onPressed,
          child: Text('Sign Up'),
          textColor: Colors.white),
      ),
    );
  }

  Widget _buildOutlineButton(void onPressed(), double w) {
    return ButtonTheme(
      minWidth: w - 32*2,
      height: 48,
      child: OutlineButton(
          onPressed: onPressed,
          child: Text('Log in'),
          color: Colors.blue,
          borderSide: BorderSide(
            color: Colors.blue,
          ),
          textColor: Colors.blue),
    );
  }


  @override
  Widget build(BuildContext context) {
    double width = MediaQuery
        .of(context)
        .size
        .width;
    return Scaffold(
      body: Column(
        children: <Widget>[
          _buildInfoWidget(context, width),
          _buildButtonWidget(context, width)
        ],
      ),
    );
  }

}



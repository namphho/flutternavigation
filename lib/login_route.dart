import 'package:flutter/material.dart';
import 'package:flutter_app_navigation/profile_route.dart';

const edge16 = EdgeInsets.all(16.0);

class LoginRoute extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _LoginState();
  }
}

class _LoginState extends State<LoginRoute>{

  bool isHide = true;
  bool isValidEmail = true;
  bool isValidPassword = true;
  String email = "";
  String password = "";

  @override
  void initState() {

  }

  void _showErrorDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Alert"),
          content: new Text("Username or Password is invalid. Please try again"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }


  Widget _buildLoginSection(double width) {
    return Container(
      padding: edge16,
      child: Column(
        children: <Widget>[
          //create email text field
          _createTextFiled('Email',(email) {
            this.email = email;
          }, TextInputAction.continueAction),
          _createPassTextFiled('Password', (pass) {
            //this.password = pass;
          }),
          _buildRaisedButton('Log in',(){
            if (isValidEmail && isValidPassword) {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ProfileRoute(email: email))
              );
            } else {
              _showErrorDialog();
            }
          }, width)
        ],
      ),
    );
  }

  Widget _createTextFiled(String hintText, void _onChanged(String value),
      TextInputAction action) {
    return Container(
        margin: EdgeInsets.only(left: 16, right: 16, bottom: 12),
        color: Colors.grey[200],
        child: TextField(
            textInputAction: action,
            onChanged: (text) {
              _onChanged(text);
              setState(() {
                isValidEmail = validateEmail(text);
              });
            },
            decoration: InputDecoration(
                contentPadding: const EdgeInsets.all(16),
                hintText: hintText,
                errorText: isValidEmail ? null : "Enter valid email",
                enabledBorder: const OutlineInputBorder(
                  // width: 0.0 produces a thin "hairline" border
                  borderSide: const BorderSide(color: Color(0xFFEEEEEE), width: 0.0),
                ),
                border: const OutlineInputBorder()
            )
        )
    );
  }

  Widget _createPassTextFiled(String hintText, void _onChanged(String value),
      ) {
    return Container(
        margin: EdgeInsets.only(left: 16, right: 16, bottom: 12),
        color: Colors.grey[200],
        child: TextField(
            obscureText: isHide,
            textInputAction: TextInputAction.done,
            onChanged: (text) {
              _onChanged(text);
              setState(() {
                isValidPassword=validatePassword(text);
              });
            },
            decoration: InputDecoration(
                suffixIcon: IconButton(icon: Icon(isHide ? Icons.visibility_off : Icons.visibility),
                    onPressed: (){
                      setState(() {
                        isHide = !isHide;
                      });
                      print('click');
                }),
                contentPadding: const EdgeInsets.all(16),
                hintText: hintText,
                errorText: isValidPassword ? null : "Invalid password, minimun lenght: 8",
                enabledBorder: const OutlineInputBorder(
                  // width: 0.0 produces a thin "hairline" border
                  borderSide: const BorderSide(color: Color(0xFFEEEEEE), width: 0.0),
                ),
                border: const OutlineInputBorder()
            )
        )
    );
  }

  bool validateEmail(String value) {
    if (value.isEmpty){
      return true;
    }
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return false;
    else
      return true;
  }

  bool validatePassword(String value){
    if (value.isEmpty){
      return true;
    }
    return value.length >= 8;
  }

  Widget _buildRaisedButton(String title, void onPressed(), double w) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: ButtonTheme(
        minWidth: double.infinity,
        height: 48,
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
            gradient: LinearGradient(colors: <Color>[Colors.blue[300], Colors.blue[700]]),
          ),
          child: FlatButton(
              onPressed: onPressed,
              child: Text(title),
              textColor: Colors.white),
        ),
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
        //logo
          Padding(
            padding: const EdgeInsets.only(bottom: 18),
            child: Image.asset('assets/ninja.png', width: 72, height: 72),
          ),
        //body
          _buildLoginSection(w)],
        //trail
      ),
    );
  }



}


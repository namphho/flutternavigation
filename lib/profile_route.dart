import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ProfileRoute extends StatefulWidget {
  final String email;

  const ProfileRoute({@required this.email}) : assert(email != null);

  @override
  _ProfileRoute createState() {
    return _ProfileRoute();
  }
}

class _ProfileRoute extends State<ProfileRoute> {
  //build profile widget
  Widget _buildProfile() {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: <Color>[Colors.blue[300], Colors.blue[700]])),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 7,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                //image
                new Container(
                  height: 72,
                  width: 72,
                  decoration: new BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/avatar_2.jpg'),
                      fit: BoxFit.cover,
                    ),
                    shape: BoxShape.circle,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(6.0),
                  child: Text(
                    widget.email,
                    style: TextStyle(color: Colors.white, fontSize: 16),
                  ),
                ),
                Text(
                  'Developer',
                  style: TextStyle(
                      color: Colors.grey[300],
                      fontSize: 14,
                      fontWeight: FontWeight.bold),
                )
              ],
            ),
          ),
          Expanded(
            flex: 3,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Text(
                      'Wife',
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                    Text(
                      '1',
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ],
                ),
                Column(
                  children: <Widget>[
                    Text(
                      'Lover',
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                    Text(
                      '1.000.000',
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  List<Widget> _buildInterestedChip() {
    List<Color> colors = <Color>[
      Colors.green[300],
      Colors.red[300],
      Colors.yellow[800],
      Colors.orange[300],
      Colors.pink[300],
      Colors.purple[300],
      Colors.amber[300],
      Colors.blue[300],
      Colors.green[300],
    ];
    List<String> title = <String>[
      'Koltin',
      'Java',
      'Dart',
      'Flutter',
      'SOLID',
      'Clean Architecture',
      'Design Pattern',
      'JavaScript'
    ];
    List<Widget> result = [];
    for (int i = 0; i < 8; i++) {
      var c = Chip(
          label: Text(title[i], style: TextStyle(color: Colors.white)),
          backgroundColor: colors[i],
          shape: StadiumBorder(side: BorderSide(color: Colors.transparent)));
      result.add(c);
    }
    return result;
  }

  //build interest
  Widget _buidInterest(BuildContext context, double width) {
    return Container(
        padding: EdgeInsets.all(16),
        child: Column(
          children: <Widget>[
            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(bottom: 16),
              child: Text(
                'Interested',
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: Colors.black54),
              ),
            ),
            Wrap(
              direction: Axis.horizontal,
              spacing: 12,
              children: _buildInterestedChip(),
            ),
            Expanded(
              flex: 1,
              child: Container(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: _buildRaisedButton('Back', (){
                    Navigator.pop(context);
                  }, width),
                ),
              ),
            ),
          ],
        ));
  }

  //_build raised button
  Widget _buildRaisedButton(String title, void onPressed(), double w) {
    return ButtonTheme(
      minWidth: w - 32*2,
      height: 48,
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(colors: <Color>[Colors.blue[300], Colors.blue[700]]),
          borderRadius: BorderRadius.all(Radius.circular(8.0))
        ),
        child: FlatButton(
            onPressed: onPressed,
            child: Text(title),
            textColor: Colors.white),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: AppBar(
        title: Text('Profile'),
        elevation: 0,
        flexibleSpace: Container(
          decoration: new BoxDecoration(
            gradient: new LinearGradient(
                colors: [
                  Colors.blue[300], Colors.blue[700]
                ],
                begin: const FractionalOffset(0.0, 0.0),
                end: const FractionalOffset(1.0, 0.0),
                stops: [0.0, 1.0],
                tileMode: TileMode.clamp),
          ),
        ),
      ),
      body: Column(
        children: <Widget>[
          //profile
          Expanded(flex: 4, child: _buildProfile()),
          //interest widget
          Expanded(
              flex: 6,
              child: Container(
                color: Colors.white,
                child: _buidInterest(context, w),
              )),
        ],
      ),
    );
  }
}
